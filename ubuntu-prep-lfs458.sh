#!/bin/bash

# LFS458 VM prep script

#update and upgrade
export DEBIAN_FRONTEND=noninteractive
apt-get update -y
apt-get upgrade -y
#add basic tools
apt-get install -y vim curl apt-transport-https vim git wget software-properties-common lsb-release ca-certificates -y

#turn off swap
swapoff -a

#load modules
cat <<EOF |  tee /etc/modules-load.d/k8s.conf
overlay
br_netfilter
EOF

modprobe -a overlay br_netfilter


cat << EOF | tee /etc/sysctl.d/kubernetes.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
sysctl --system

#install docker repo
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg \
| sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo \
"deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] \
https://download.docker.com/linux/ubuntu \
$(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

# install containerd
apt-get update && apt-get install containerd.io -y
containerd config default | tee /etc/containerd/config.toml
sed -e 's/SystemdCgroup = false/SystemdCgroup = true/g' -i /etc/containerd/config.toml
systemctl restart containerd

curl -fsSL https://pkgs.k8s.io/core:/stable:/v1.29/deb/Release.key | sudo gpg --dearmor -o /etc/apt/keyrings/kubernetes-apt-keyring.gpg
echo 'deb [signed-by=/etc/apt/keyrings/kubernetes-apt-keyring.gpg] https://pkgs.k8s.io/core:/stable:/v1.29/deb/ /' | sudo tee /etc/apt/sources.list.d/kubernetes.list

apt-get update
apt-get install -y kubeadm=1.29.0-1.1 kubelet=1.29.0-1.1 kubectl=1.29.0-1.1

apt-mark hold kubelet kubeadm kubectl

# on control plane node only
# kubeadm init --pod-network-cidr  192.168.0.0/16 --apiserver-bind-port 443 --kubernetes-version 1.27.1 | tee kubeadm-init.out
# kubeadm join 172.31.18.144:443 --token q6ygdx.r0vh26cuepxtmj3n --discovery-token-ca-cert-hash sha256:0c7e0912ab17b717a1f2fd3cf0996dde5a5c06029140131c5d2a14cf0828b7a6 

